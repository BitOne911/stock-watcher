import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {StockDashboardPageComponent} from "./pages/stock-dashboard-page/stock-dashboard-page.component";

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/dashboard' },
  { path: 'dashboard', component: StockDashboardPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
