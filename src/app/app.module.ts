import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule} from "@angular/forms";
import { StockDashboardComponent } from './features/dashboard/stock-dashboard/stock-dashboard.component';
import { StockTickerComponent } from './features/dashboard/stock-ticker/stock-ticker.component';
import { StockDashboardPageComponent } from './pages/stock-dashboard-page/stock-dashboard-page.component';

@NgModule({
  declarations: [
    AppComponent,
    StockDashboardComponent,
    StockTickerComponent,
    StockDashboardPageComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
