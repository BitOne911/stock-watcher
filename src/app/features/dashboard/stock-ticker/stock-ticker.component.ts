import {Component, Input, OnInit} from '@angular/core';
import {Stock} from "../../../core/interfaces/stock";

@Component({
  selector: 'app-stock-ticker',
  templateUrl: './stock-ticker.component.html',
  styleUrls: ['./stock-ticker.component.sass']
})
export class StockTickerComponent implements OnInit {

  @Input() stock!: Stock;

  constructor() { }

  ngOnInit(): void {
  }

  public getPixelPlacementOfArrow(price: number, high: number, low: number) : string {
    const normalizedPixelRange = 105;
    const yPriceRange = high - low;
    const highPriceCurrentPriceDiff = high - price;
    const yArrowPoint = yPriceRange - highPriceCurrentPriceDiff;
    //Figuring out where in the range it lies. If range is 20 in the scale, and difference between high price and current price is 0,
    // then its at 20 (top of scale) which should be 105px from bottom
    return Math.round((normalizedPixelRange * yArrowPoint) / yPriceRange) + 'px';
  }
}
