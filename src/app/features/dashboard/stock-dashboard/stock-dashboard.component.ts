import { Component, OnInit } from '@angular/core';
import {Stock} from "../../../core/interfaces/stock";

const stocks = [
  {
    name: 'ALPHABET INC. CL C',
    symbol: 'GOOG',
    price: 706.32,
    changeInPrice: 15.32,
    percentChange: 2.22,
    open: 691,
    high: 709.28,
    low: 689.47
  }, {
    name: 'YAHOO! INC',
    symbol: 'YHOO',
    price: 29.27,
    changeInPrice: -0.01,
    percentChange: -0.03,
    open: 29.28,
    high: 29.66,
    low: 29.06
  }, {
    name: 'American International Group Inc',
    symbol: 'AIG',
    price: 53.08,
    changeInPrice: 1.02,
    percentChange: 1.96,
    open: 52.06,
    high: 53.47,
    low: 52.28
  }, {
    name: 'VelocityShares 3x Long Crude Oil',
    symbol: 'UWTIF',
    price: 1.61,
    changeInPrice: .24,
    percentChange: 16.55,
    open: 1.37,
    high: 1.74,
    low: 1.50
  }, {
    name: '3X INVERSE CRUDE',
    symbol: 'DWTIF',
    price: 253.41,
    changeInPrice: -53.69,
    percentChange: -17.48,
    open: 307.10,
    high: 297.50,
    low: 245.59
  }, {
    name: 'GROUPON INC',
    symbol: 'GRPN',
    price: 3.74,
    changeInPrice: -.34,
    percentChange: -8.33,
    open: 4.08,
    high: 4.13,
    low: 3.60,
    styleBottomPixels: ''
  }];

@Component({
  selector: 'app-stock-dashboard',
  templateUrl: './stock-dashboard.component.html',
  styleUrls: ['./stock-dashboard.component.sass']
})
export class StockDashboardComponent implements OnInit {

  txtStockSymbol: string = '';
  watchedStocks: Stock[] = [];

  constructor() {
  }

  ngOnInit(): void {
  }

  public addStock(stockSymbol : string): void {
    const upperCaseSymbol = stockSymbol.toUpperCase();
    const stock = stocks.find(element => element.symbol == upperCaseSymbol);
    if(stock !== undefined) {
      this.txtStockSymbol = '';
      this.pushNewWatchedStock(stock);
    } else {
      alert('Stock symbol not found');
    }
  }

  private pushNewWatchedStock(stock : Stock) : void {
    const isWatched = this.isStockWatched(stock.symbol);
    if(isWatched) {
      alert('Stock is already being watched');
    } else {
      this.watchedStocks.push(stock);
    }
  }

  private isStockWatched(stockSymbol : string) : boolean {
    const stock = this.watchedStocks.find(element => element.symbol == stockSymbol);
    return stock === undefined ? false : true;
  }


}
