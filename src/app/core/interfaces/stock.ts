export interface Stock {
  name: string,
  symbol: string,
  price: number,
  changeInPrice: number,
  percentChange: number,
  open: number,
  high: number,
  low: number
}
